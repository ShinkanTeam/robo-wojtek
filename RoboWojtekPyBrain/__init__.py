import pyparsing as pp

# Grammar definition.

    # Static conceptions.

# Colours recognized by robots' camera.
colour = (pp.Literal('red') | pp.Literal('green') | pp.Literal('blue')
    | pp.Literal('white') | pp.Literal('black'))

# Concepts of numbers.
integer = pp.Word(pp.nums)
number = integer.setResultsName('Number')
# Concept of name.
name = (pp.NotAny(pp.Literal("is") | pp.Literal("and") | pp.Literal("there")) +
    (pp.Word(pp.alphas) | number))

# Concepts of statements availible
    # Rooms statements
room1 = ('there is a room named') + name.setResultsName('Name')
room2 = name.setResultsName('Name') + ('is a room')
roomSta = pp.Group(room1 | room2).setResultsName('Room')

    # Doors statements
door1 = (('there is door between ') + name.setResultsName('Name') + ('and') +
    name.setResultsName('Name'))
doorSta = pp.Group(door1).setResultsName('Door')

    # Object creation statements
obj1 = ('object') + name.setResultsName('Name') + ('exist')
objSta = pp.Group(obj1).setResultsName('NewObject')

    # Defining object
defObj1 = (pp.Optional(pp.Literal('the') | pp.Literal('a') | pp.Literal('an'))
    + name.setResultsName('ObjName') + ('is') + colour.setResultsName(
    'Colour') + ("and it's placed in") + pp.Optional(pp.Literal('the')
    | pp.Literal('a') | pp.Literal('an')) + name.setResultsName('RoomName'))
defObjSta = pp.Group(defObj1).setResultsName('DefineObject')

# ordThings
ordTime = number + pp.Literal('seconds')
ordDistance = pp.Group(number + (pp.Literal('centimetres') | (pp.Literal(
    'centimetre'))))
ordAngle = pp.Group(number + pp.Literal('degrees'))
ordDirectionFB = (pp.Literal('front') | pp.Literal('back'))
ordDirectionLR = (pp.Literal('left') | pp.Literal('right'))
ordRW = pp.Literal('yourself')

# Concepts of activities availible for robot. Represented as Orders.
jedz = pp.Group((pp.Literal('drive') | pp.Literal('go') | pp.Literal('run')
    ).setResultsName('Jedz')).setResultsName('Action') + ((pp.Group(ordTime)
    .setResultsName('Time') | ordDistance.setResultsName('Distance'))
    + pp.Optional(ordDirectionFB.setResultsName('Direction')))

podjedz = (pp.Group(pp.Group((pp.Literal('drive') | pp.Literal('go')
    | pp.Literal('run')) + (pp.Literal('untill') | 'till')).setResultsName(
    'Podjedz')).setResultsName('Action') + ('you are') + ordDistance
    .setResultsName('Distance'))

obroc = (pp.Group((pp.Literal('turn')).setResultsName('Obroc'))
    .setResultsName('Action') + ordDirectionLR.setResultsName('Direction')
    + ((ordAngle).setResultsName('Angle') | pp.Group(pp.Literal('for')
    + ordTime).setResultsName('Time')))

zawroc = pp.Group(pp.Group(pp.Literal('turn') + (pp.Literal('back')
    | pp.Literal('around')) + pp.Optional(pp.Literal('yourself'))
    ).setResultsName('Zawroc')).setResultsName('Action')

ustaw = (pp.Group(pp.Group(pp.Literal('set') + pp.Optional(pp.Literal('the')
    | pp.Literal('a'))).setResultsName('Ustaw')).setResultsName('Action') +
    (pp.Group('rotation speed at').setResultsName('TurnSpeed') | pp.Group(
    'speed to').setResultsName('Speed')) + number)

sledz = pp.Literal('follow').setResultsName('Sledz')

patrz = pp.Literal('look').setResultsName('Patrz')

kontynuuj = pp.Literal('continue').setResultsName('Kontynuuj')

# ordThing
#ordThing = (ordTime.setResultsName('Time') | ordDistance.setResultsName(
#    'Distance') | ordAngle.setResultsName('Angle') | ordRW.setResultsName('RW'))

# Concepts of questions
    # Existance of the room
exiRo1 = ("is there a room" + name.setResultsName('Name'))
exiRoQ = pp.Group(exiRo1).setResultsName("RoomExist")

    # Existance of the object
exiOb1 = ("is there an object" + name.setResultsName('Name'))
exiObQ = pp.Group(exiOb1).setResultsName("ObjExist")

    # Existance of the object in a room
colObjInR1 = (pp.Literal("is") + name.setResultsName('Colour') + name
    .setResultsName('Name') + "in" + name.setResultsName('Room'))
colObjInRQ = pp.Group(colObjInR1).setResultName("ObjectInRoom")

    # List of availible rooms
avaRo1 = ("what rooms do you know")
avaRoQ = pp.Group(avaRo1).setResultsName("AvaRooms")

    # Find a way from room1 to room2
way1 = (("what's the way from") + pp.Optional(pp.Literal('the') | pp.Literal(
    'a') | pp.Literal('an')) + name.setResultsName('RoomName1') + "to"
    + pp.Optional(pp.Literal('the') | pp.Literal('a') | pp.Literal('an'))
    + name.setResultsName('RoomName2'))
wayQ = pp.Group(way1).setResultsName("FindWay")

    # List of objects in room
avaObIR1 = ("what are the objects in") + name.setResultsName('RoomName')
avaObIRQ = pp.Group(avaObIR1).setResultsName("ObjectsInRoom")

    # List of availible commends
possible = ("what can i do")
possibleQ = pp.Group(possible).setResultsName("Possible")

# Help concepts
helpUser = pp.Group(pp.Literal('help') + pp.Optional(pp.restOfLine
    ).setResultsName('Topic')).setResultsName('Help')

    # Concepts of sentences.

statement = pp.Group(roomSta | doorSta | objSta | defObjSta
    ).setResultsName('Statement')

order = pp.Group(jedz | podjedz | obroc | zawroc | ustaw
    ).setResultsName('Order')

question = pp.Group((exiRoQ | exiObQ | avaRoQ | wayQ | possibleQ | avaObIRQ
    | colObjInRQ) + pp.Literal('?')).setResultsName('Question')

sentence = statement | order | question

longSentence = pp.Group((((statement | order) + pp.ZeroOrMore("," + (statement
    | order))) + "and" + sentence) | (sentence | helpUser)
    ).setResultsName('Sentence')


# Parsing

# TESTS:
    # Random shit:
#text = "blablablablabla"

    # Statements:
        # Add new room
#text = "there is a room named kitchen"
        # Add new door
#text = "there is door between kitchen and saloon"
        # Add new object
#text = "object chair exist"
        # Define existing object
#text = "chair is red and it's placed in kitchen"
        # Create AND Define at the same time

    # Orders:
        # JEDZ
#text = "go 25 centimetres"
#text = "drive 24 centimetres back"
#text = "run 10 seconds front"
#text = "go 90 seconds front"

        # PODJEDZ
#text = "go till you are 10 centimetres from something"
#text = "go till you are 25 centimetres from something"
#text = "go till you are 30 centimetres from something"
    # wrong number test
#text = "go till you are -10 centimetres from something"

        # OBROC
#text = "turn left for 90 seconds"
#text = "turn right 90 degrees"

        # ZAWROC
#text = "turn back yourself"

        # USTAW
#text = "set the speed to 10"
#text = "set the rotation speed at 5"

        # ODCZYT
#text = ""

    # Questions:
        # Existance of room
#text = "is there a room kitchen?"

        # Existance of object
#text = "is there an object ball?"

        # List of existing rooms
#text = "what rooms do you know?"

        # What's the way from room1 to room2?
#text = "What's the way from kitchen to balcony?"

        # What are the objects in the room?
#text = "What are the objects in kitchen?"

        # What can user do?
#text = "what can I do?"

    # Long Sentences:
#text = "Go 500 centimetres and turn back yourself"
#text = "Go 500 centimetres and there is a room named kitchen"
#text = "Go 500 centimetres, there is a room named kitchen and turn back yourself"
#text = "Go 500 centimetres, there is a room named kitchen, turn back yourself and object chair exists"
#text = "Go 500 centimetres, object chair exist and what can I do?"
#text = "Go 500 centimetres, there is a room named kitchen, turn back yourself, object chair exist, chair is red and it's placed in kitchen and what rooms do you know?"

    # Help:
#text = "help"
#text = "help room"

try:
    parseResult = longSentence.parseString(text.lower()).asXML()

    #print parseResult

    import xml.etree.ElementTree as ET
    root = ET.fromstring(parseResult)

    result = []
    answer = ""

    part = 0
    orders = {}
    ordc = 0
    question = ""
    newData = []
    newc = 0

    for wholeSent in root:
        # Wlasciwa petla po calej wypowiedzi
        for sentence in wholeSent:
            if sentence.tag == 'Statement':
                newc = newc + 1
                newStatement = ""
                sentType = ""
                for child in sentence:
                    sentType = child.tag
                    if sentType == "Room":
                        roomName = ""
                        for childDeep in child:
                            if childDeep.tag == "Name":
                                roomName = childDeep.text
                        newStatement = ("assert(pokoj(" + roomName
                            + "))")
                    if sentType == "Door":
                        roomNames = []
                        for childDeep in child:
                            if childDeep.tag == "Name":
                                roomNames.append(childDeep.text)
                        newStatement = ("assert(drzwi(" + roomNames[0]
                            + ", " + roomNames[1] + "))")
                    if sentType == "NewObject":
                        objName = ""
                        for childDeep in child:
                            if childDeep.tag == "Name":
                                objName = childDeep.text
                        newStatement = ("assert(obiekt(" + objName + "))")
                    if sentType == "DefineObject":
                        objName = ""
                        colour = ""
                        roomName = ""
                        for childDeep in child:
                            if childDeep.tag == "ObjName":
                                objName = childDeep.text
                            if childDeep.tag == "RoomName":
                                roomName = childDeep.text
                            if childDeep.tag == "Colour":
                                colour = childDeep.text
                        newStatement = ("assert(koloru_nalezy_do(" + objName
                            + ", " + colour + ", " + roomName + "))")
                result.append({str(part) + "Sentence" + str(newc):
                    newStatement})
                newData.append(newStatement)
                newData.append("zapisz")
                part = part + 1
            if sentence.tag == 'Order':
                ordc = ordc + 1
                newOrder = {}
                for child in sentence:
                    if child.tag == 'Action':
                        for childDeep in child:
                            newOrder.update({"Type": childDeep.tag})
                if newOrder["Type"] == 'Jedz':
                    newOrder.update({'Direction': True})
                    for child in sentence:
                        if child.tag == 'Direction':
                            if child.text == 'front':
                                newOrder.update({'Direction': True})
                            else:
                                newOrder.update({'Direction': False})
                        if child.tag == 'Distance':
                            newOrder.update({"Condition": "Distance"})
                            for childDeep in child:
                                if childDeep.tag == 'Number':
                                    newOrder.update({'Value':
                                        childDeep.text})
                        if child.tag == 'Time':
                            newOrder.update({"Condition": "Time"})
                            for childDeep in child:
                                if childDeep.tag == 'Number':
                                    newOrder.update({'Value': childDeep.text})
                elif newOrder["Type"] == 'Podjedz':
                    for child in sentence:
                        if child.tag == 'Distance':
                            for childDeep in child:
                                if childDeep.tag == 'Number':
                                    if int(childDeep.text) < 25:
                                        newOrder.update({'ToDistance': str(25)})
                                    else:
                                        newOrder.update({'ToDistance':
                                            childDeep.text})
                elif newOrder["Type"] == 'Obroc':
                    for child in sentence:
                        for deeper in child:
                            if child.tag == 'Direction':
                                if child.text == 'left':
                                    newOrder.update({'Direction': True})
                                else:
                                    newOrder.update({'Direction': False})
                            if child.tag == 'Angle':
                                newOrder.update({"Condition": "Angle"})
                                for childDeep in child:
                                    if childDeep.tag == 'Number':
                                        newOrder.update({'Value':
                                            childDeep.text})
                            if child.tag == 'Time':
                                newOrder.update({"Condition": "Time"})
                                for childDeep in child:
                                    if childDeep.tag == 'Number':
                                        newOrder.update({'Value':
                                            childDeep.text})
                elif newOrder["Type"] == 'Ustaw':
                    for child in sentence:
                        if child.tag == 'TurnSpeed':
                            newOrder.update({'ToSet': "TurnSpeed"})
                        else:
                            newOrder.update({'ToSet': "Speed"})
                        if child.tag == 'Number':
                            newOrder.update({'Value': child.text})
                result.append({str(part) + "Order" + str(ordc): newOrder})
                orders.update({str(ordc): newOrder})
                part = part + 1
            if sentence.tag == 'Question':
                newQuestion = ""
                queType = ""
                for child in sentence:
                    part = part + 1
                    queType = child.tag
                    if queType == "RoomExist":
                        roomName = ""
                        for childDeep in child:
                            if childDeep.tag == "Name":
                                roomName = childDeep.text
                        newQuestion = ("pokoj(" + roomName + ")")
                        result.append({str(part - 1) + "Question": newQuestion})
                        question = newQuestion
                    if queType == "ObjExist":
                        roomName = ""
                        for childDeep in child:
                            if childDeep.tag == "Name":
                                roomName = childDeep.text
                        newQuestion = ("obiekt(" + roomName + ")")
                        result.append({str(part - 1) + "Question": newQuestion})
                        question = newQuestion
                    if queType == "AvaRooms":
                        newQuestion = ("pokoj(X)")
                        result.append({str(part - 1) + "Question": newQuestion})
                        question = newQuestion
                    if queType == "ObjectInRoom":
                        colour = ""
                        objectName = ""
                        roomName = ""
                        for childDeep in child:
                            if childDeep.tag == "Colour":
                                colour = childDeep.text
                            if childDeep.tag == "Name":
                                objectName = childDeep.text
                            if childDeep.tag == "Room":
                                roomName = childDeep.text
                        newStatement = ("assert(koloru_nalezy_do(" + objName
                            + ", " + colour + ", " + roomName + "))")
                        newQuestion = ("koloru_nalezy_do(" + objectName + ","
                            + colour + "," + roomName + ")")
                        result.append({str(part - 1) + "Question": newQuestion})
                        question = newQuestion
                    if queType == "FindWay":
                        roomName1 = ""
                        roomName2 = ""
                        for childDeep in child:
                            if childDeep.tag == "RoomName1":
                                roomName1 = childDeep.text
                            if childDeep.tag == "RoomName2":
                                roomName2 = childDeep.text
                        newQuestion = ("droga(" + roomName1 + "," + roomName2
                            + ")")
                        result.append({str(part - 1) + "Question": newQuestion})
                        question = newQuestion
                    if queType == "ObjectsInRoom":
                        roomName = ""
                        for childDeep in child:
                            if childDeep.tag == "RoomName":
                                roomName = childDeep.text
                        newQuestion = ("wszystkieobiektywpokoju(" + roomName
                            + ")")
                        result.append({str(part - 1) + "Question": newQuestion})
                        question = newQuestion
                    if queType == "Possible":
                        answer = ("Possible topics: go, go till, rotate, turn "
                            + "back, set, room, door, object. If You want to "
                            + "know more type 'help TOPIC'.")
                        part = part - 1
            if sentence.tag == 'Help':
                for child in sentence:
                    topic = ""
                    if child.tag == "Topic":
                        topic = child.text
                    if topic == " go":
                        answer = ("Order to move. Examples of correct senten" +
                            "ces:\n(Drive OR Go OR Run) NUMBER (seconds OR c" +
                            "entimetres) (OPTIONAL front OR back).")
                    elif topic == " go till":
                        answer = ("Order to go till Robo-Wojtek see somethin" +
                            "g. Distance must be at least 25 centimetres. If" +
                            " you write less it will be changed to 25. Examp" +
                            "les of correct sentences:\n(Drive OR Go OR Run)" +
                            " (untill OR till) you are NUMBER centimetres fr" +
                            "om something")
                    elif topic == " rotate":
                        answer = ("Order to rotate. Examples of correct sent" +
                            "ences:\n(Turn) (left OR right) ((NUMBER degrees" +
                            ") OR (for NUMBER seconds)).")
                    elif topic == " turn back":
                        answer = ("Order to turn back(turn 180 degrees). Exa" +
                            "mples of correct sentences:\nTurn (around OR ba" +
                            "ck) (OPTIONAL yourself)")
                    elif topic == " set":
                        answer = ("Order to set speed or rotation speed of R" +
                            "obo-Wojtek. Examples of correct sentences:\nSet" +
                            " (OPTIONAL the OR a) ((rotation speed at) OR (s" +
                            "peed to)) NUMBER.")
                    elif topic == " room":
                        answer = ("Adding the fact of existance of room to t" +
                            "he memory of Robo-Wojtek. Examples of correct s" +
                            "entences:\nThere is a room named NAME.\nNAME is" +
                            " a room.\n\nQuestion whether the room with the " +
                            "given name exists. Examples of correct sentence" +
                            "s:\nIs there a room NAME?\n\nListing the object" +
                            "s from the room. Examples of correct sentences:" +
                            "\nWhat are the objects in ROOM?\n\nQuestion whe" +
                            "ther the object of given colour is in the room." +
                            " Examples of correct sentences:\nIs COLOUR OBJE" +
                            "CT in ROOM?")
                    elif topic == " door":
                        answer = ("Adding the fact of existance of door betw" +
                            "eeen two rooms to the memory of Robo-Wojtek. Ex" +
                            "amples of correct sentences:\nThere is door bet" +
                            "ween NAME and NAME.")
                    elif topic == " object":
                        answer = ("Adding the fact of existance of object to" +
                            " the memory of Robo-Wojtek. Examples of correct" +
                            " sentences:\nObject NAME exist.\n\nSetting up w" +
                            "here is this object placed and what's its colou" +
                            "r. Examples of correct sentences:\n(OPTIONAL Th" +
                            "e OR A OR An) OBJECTNAME is COLOUR and it's pla" +
                            "ced in (OPTIONAL the OR a OR an) ROOM\n\nQuesti" +
                            "on whether the object with the given name exist" +
                            "s. Examples of correct sentences:\nIs there an " +
                            "object NAME?")
                    elif topic == " help":
                        answer = ("Nie moge Ci pomoc. Jestem Koniem!")
                    else:
                        answer = ("Possible topics: go, go till, rotate, tur" +
                            "n back, set, room, door, object. If You want to" +
                            " know more type 'help TOPIC'.")

    if (ordc > 0):
        orders.update({"Type": "Navigation"})
        orders.update({"Count": str(ordc)})

    print "\n"

    result = str(result)
    newData = str(newData)
    orders = str(orders)

    print "Statements: " + newData
    print "Orders: " + orders
    print "Question: " + question
    print "\nAnswer: " + answer
except pp.ParseException:
    answer = "I don't understand what you wrote."
    result = "I don't understand what you wrote."
except:
    import sys
    print "Unexpected error:", sys.exc_info()[0]
    raise

print "\n" + result + "\n"