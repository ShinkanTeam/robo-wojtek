%pokoje
:- dynamic pokoj/1.

pokoj(hall).
pokoj(salon).
pokoj(kitchen).
pokoj(bedroom1).
pokoj(bedroom2).

%drzwi
:- dynamic drzwi/2.

drzwi(hall, salon).
drzwi(hall, kitchen).
drzwi(hall, bedroom1).
drzwi(hall, bedroom2).
drzwi(bedroom2, bedroom1).

%obiekty znajdujace sie w pokojach
:- dynamic obiekt/1.

obiekt(chair).
obiekt(table).
obiekt(couch).

%lokalizajca danego obiektu
:- dynamic koloru_nalezy_do/3.

koloru_nalezy_do(couch, red, bedroom1).
koloru_nalezy_do(couch, brown, bedroom2).
koloru_nalezy_do(table, brown, kitchen).
koloru_nalezy_do(chair, brown, kitchen).
koloru_nalezy_do(chair, brown, kitchen).
koloru_nalezy_do(chair, brown, kitchen).
koloru_nalezy_do(chair, brown, kitchen).

