package Robot;

import json.JSONException;
import json.JSONObject;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.addon.NXTCam;
import java.awt.Rectangle;


public class SensorModule extends Thread {
    private UltrasonicSensor distanceSensor;
    private static SensorModule instance;
    private boolean end = false;
    private NXTCam cam;
    private int x;
    private int objectID;
    private int width;
    
    public SensorModule() {
    	distanceSensor = new UltrasonicSensor(SensorPort.S4);
    	cam = new NXTCam(SensorPort.S1);
    	cam.enableTracking(true);
    	cam.setTrackingMode(NXTCam.OBJECT_TRACKING);
    }
    
    public static SensorModule getInstance() {
        if(instance == null)
            instance = new SensorModule();
        return instance;
    }
    
    @Override
    public void run() {
    	while(!end) {
    	}
    }
    
    public void stopSensors() {
    	end = true;
    }
    
    public void executeCommands(JSONObject commands) throws JSONException {
    	String type = commands.getString("SensorType");
    	switch(type) {
	    	case "Distance":
	    		sendDistance();
	    		break;
    	}
    }
    
    public int getDistanceToObject() {
    	return distanceSensor.getDistance();
    }
    
    private void sendDistance() {
    	String msg = "Distance to the object before me is "+distanceSensor.getDistance()+" cm.";
    	BTOutput.getInstance().setOutputData(msg.getBytes());
    }

    private int whatSee() {
    	return cam.getNumberOfObjects();
    }


    private int see(int color) {
    	int id = -1;
    	for (int i=0; i<whatSee(); i++)
    		if (cam.getObjectColor(i)==color)
    		{
    			Rectangle rect = cam.getRectangle(id);
    			if(rect.width*rect.height >= 100)
    				id = i;
    		}
    	return id;
    }


	public String checkObjectPosition(String color) {
		int colorID = -1;
		String ret = "Left";
		switch(color) {
			case "Red":
				colorID = 0;
				break;
			case "Blue":
				colorID = 1;
				break;
			case "Green":
				colorID = 2;
				break;
			case "Yellow":
				colorID = 3;
				break;
			case "Cyan":
				colorID = 4;
				break;
			case "Magenta":
				colorID = 5;
				break;
			case "Black":
				colorID = 6;
				break;
			case "White":
				colorID = 7;
				break;
		}
		objectID = see(colorID);
		if(objectID != -1) {
			Rectangle rect = cam.getRectangle(objectID);
			int newX = rect.x;
			int newWidth = rect.width;
			if(newX < x)
				ret =  "Left";
			else if(newX > x)
				ret =  "Right";
			else if(newX == 0 && newWidth < width)
				ret =  "Left";
			else if(newX == 179 && newWidth < width)
				ret =  "Right";
			else if (newX == x)
				ret =  "Forward";
			x = newX;
			if(x == 0)
				x = 1;
			else if(x == 179)
				x = 178;
			width = newWidth;
		}
		return ret;
	}
	
	public void trackNewObject() {
		x = -1;
		width = 0;
	}
}
