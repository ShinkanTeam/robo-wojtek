package Robot;

import json.JSONException;
import json.JSONObject;
import lejos.nxt.Sound;


public class RoboWojtek {
    private boolean end;
    
    public void start() {
        end = false;
        Navigator.getInstance().start();
        SensorModule.getInstance().start();
        while(!end) {
            JSONObject commands = BT.getInstance().getNewCommands();
            String type = "";
            try {
            	type = commands.getString("Type");
            } catch(JSONException ex) {
                LCDScreen.drawString("Blad odczytu Type z JSON." +ex.getMessage());
                Sound.beepSequence();
            }
            switch(type) {
                case "Stop":
                    stop();
                    break;
                case "Navigation":
                    try {
                        Navigator.getInstance().executeCommands(commands, false);
                    } catch(JSONException ex) {
                    	LCDScreen.drawString("Blad czytania z JSON w nawigacji." +ex.getMessage());
                        Sound.beepSequence();
                    }
                    break;
                case "Sensor":
                    try {
                        SensorModule.getInstance().executeCommands(commands);
                    } catch(JSONException ex) {
                    	LCDScreen.drawString("Blad czytania z JSON w czujnikach." +ex.getMessage());
                        Sound.beepSequence();
                    }
                    break;
                case "Interruption":
                    try {
                        Navigator.getInstance().interruptSequence(commands);
                    } catch(JSONException ex) {
                    	LCDScreen.drawString("Blad odczytu JSON w przerwaniu." +ex.getMessage());
                        Sound.beepSequence();
                    }
                    break;
            }
        }
        Sound.beep();
    }
    
    public void stop() {
    	Navigator.getInstance().stopNavigator();
    	SensorModule.getInstance().stopSensors();
    	BT.getInstance().closeBluetooth();
        end = true;
    }
}
