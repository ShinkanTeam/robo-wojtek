package Robot;

import lejos.nxt.Sound;

/**
 *
 * @author Shin
 */
public class RoboMain {
    public static void main(String[] args) {
        Sound.beepSequenceUp();
        LCDScreen.drawString("Czeka na polaczenie");
        if(BT.getInstance().initiate()) {
        	LCDScreen.drawString("Polaczono");
        	Sound.beepSequenceUp();
            new RoboWojtek().start();
        }
        else
            Sound.beep();
    }

}

//ustawianie kamery!