package Robot;

import lejos.nxt.LCD;

public class LCDScreen {
	public static void drawString(String msg) {
		char[] line;
        int remaining = msg.length();
        int read;
        int lines = (int)Math.ceil((double)msg.length()/16);
        LCD.clear();
        for(int i=0; i<lines; i++) {
	        
	        if(16 > remaining)
                read = remaining;
	        else {
	            read = 16;
	            remaining -= 16;
	        }
	        line = new char[read];
	        msg.getChars(0, read, line, 0);
	        msg = msg.substring(read);
	        LCD.drawString(String.valueOf(line), 0, i);
        }
	}
}
