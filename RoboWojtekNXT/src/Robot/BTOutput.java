package Robot;

import java.io.DataOutputStream;
import java.io.IOException;
import lejos.nxt.Sound;

public class BTOutput extends Thread{
	private boolean close = false;
	private byte[] data;
	private DataOutputStream os;   	
	private static BTOutput instance;
	
	public static BTOutput getInstance() {
        if(instance == null)
            instance = new BTOutput();
        return instance;
    }
	
	public void setOutputStream(DataOutputStream outputStream) {
		os = outputStream;
	}
	
	public void run() {
		while(!close) {
			if(data != null) {
				try {
					os.write(data);
					os.flush();
					data = null;
				} catch(IOException ex) {
					LCDScreen.drawString("Blad wysylania w BT." +ex.getMessage());
		            Sound.beepSequence();
		            data = null;
				}
			}
		}
	}
	
	public void setOutputData(byte[] toSend) {
		data = toSend;
	}
	
	public void closeStream() {
		close = true;
	}
}