package Robot;

import java.util.Queue;
import json.JSONException;
import json.JSONObject;
import lejos.nxt.Motor;
import lejos.nxt.Sound;
import lejos.robotics.navigation.DifferentialPilot;

public class Navigator extends Thread {
    private DifferentialPilot pilot;
    private static Navigator instance;
    private boolean end;
    private Queue<JSONObject> commandsQueue;
    private boolean executeQueue;
    private double turnRatio = 1.16666666667;
    private boolean execute;
    
    public Navigator() {
        pilot = new DifferentialPilot(2.1f, 4.5f, Motor.B, Motor.C);
        commandsQueue = new Queue<JSONObject>();
        end = false;
    }
    
    @Override
    public void run() {
    	while(!end) {
    		while(pilot.isMoving())
				Thread.yield();
    		if(executeQueue && !commandsQueue.isEmpty()) {
    			JSONObject command = (JSONObject)commandsQueue.pop();
    			String action = "";
    			try {
    				action = command.getString("Type");
    			} catch(JSONException ex) {
    				LCDScreen.drawString("Blad JSON w navigatorze."+ex.getMessage());
    	            Sound.beepSequence();
    	    	}
    			switch(action) {
    				case "Jedz" :
    					travel(command);
    					break;
    				case "Podjedz" :
    					travelUntil(command);
    					break;
    				case "Zawroc" :
    					goBack(command);
    					break;
    				case "Obroc" :
    					turn(command);
    					break;
    				case "Ustaw" :
    					set(command);
    					break;
    				case "Sledz" :
    					follow(command);
    					break;
    				case "Odczyt" :
    					readSpeed(command);
    					break;
    			}
    		}
    	}
    }
    
    private void follow(JSONObject command) {
    	boolean ok = false;
    	String color = "";
    	int time = 0;
    	int distance = 0;
    	try {
    		color = command.getString("Color");
    		time = command.getInt("TimeToFollow");
    		distance = command.getInt("Distance");
    		ok = true;
    	} catch(JSONException ex) {
    		LCDScreen.drawString("Blad czytania z JSON w navigatorze. "+ex.getMessage());
    		Sound.beepSequence();
    	}
    	if(ok) {
    		execute = true;
    		long start = System.currentTimeMillis();
			long end, sec;
			String move;
			SensorModule.getInstance().trackNewObject();
			do {
				move = SensorModule.getInstance().checkObjectPosition(color);
				switch(move) {
					case "Left":
						pilot.arc(2.25, turnRatio, true);
						break;
					case "Right":
						pilot.arc(-2.25, turnRatio, true);
						break;
					case "Forward":
						if(SensorModule.getInstance().getDistanceToObject() > distance)
							pilot.forward();
						else
							pilot.stop();
						break;
				}
				end = System.currentTimeMillis();
				sec = (end-start)/1000;
			} while(sec<time && execute);
			pilot.stop();
			sendMessage("I'm stopping following object.");
    	}
    }
    
    private void readSpeed(JSONObject command) {
    	try {
    		String read = command.getString("ToRead");
    		switch(read) {
				case "Speed" :
					sendMessage("I am moving with speed of "+pilot.getTravelSpeed()*2+" cm/s.");
					break;
				case "TurnSpeed" :
					sendMessage("I am turning with speed of "+pilot.getRotateSpeed()+" degrees/s.");
					break;
    		}
    	} catch(JSONException ex) {
    		LCDScreen.drawString("Blad czytania z JSON w navigatorze. "+ex.getMessage());
    		Sound.beepSequence();
    	}
    }
    
    private void set(JSONObject command) {
    	String toSet = "";
    	double value = 0;
    	try {
    		toSet = command.getString("ToSet");
			value = command.getInt("Value");
		} catch(JSONException ex) {
			LCDScreen.drawString("Blad czytania z JSON w navigatorze." +ex.getMessage());
            Sound.beepSequence();
    	}
    	
    	switch(toSet) {
	    	case "Speed":
	    		pilot.setTravelSpeed(value/2);
	    		break;
	    	case "TurnSpeed":
	    		pilot.setRotateSpeed(value);
	    		break;
    	}
    }
    
    private void travel(JSONObject command) {
    	boolean direction = true;
    	String condition = "";
    	double value = 0;
    	try {
	    	direction = command.getBoolean("Direction");
	    	condition = command.getString("Condition");
	    	value = command.getInt("Value");
    	} catch(JSONException ex) {
    		LCDScreen.drawString("Blad czytania z JSON w navigatorze." +ex.getMessage());
            Sound.beepSequence();
    	}
    	
    	switch(condition) {
    		case "Time":
    			execute = true;
    			long start = System.currentTimeMillis();
    			if(direction)
    				pilot.forward();
    			else
    				pilot.backward();
    			long end;
    			long sec;
    			do {
    				end = System.currentTimeMillis();
    				sec = (end-start)/1000;
    			} while(sec<value && execute);
    			pilot.stop();
    			break;
    		case "Distance":
    			value /=2;
    			if(direction)
    				pilot.travel(value, true);
    			else
    				pilot.travel(-value, true);
    			break;
    	}
    	sendMessage("Done");
    }
    
    private void travelUntil(JSONObject command) {
    	int minDistance = 0;
    	try {
    		minDistance = command.getInt("ToDistance");
		} catch(JSONException ex) {
			LCDScreen.drawString("Blad czytania z JSON w navigatorze." +ex.getMessage());
            Sound.beepSequence();
    	}
    	
    	pilot.forward();
    	while(SensorModule.getInstance().getDistanceToObject() > minDistance)
    		;
    	pilot.stop();
    	sendMessage("Done");
    }

    private void goBack(JSONObject command) {
    	pilot.rotate(180*turnRatio);
    	sendMessage("I have turned back");
    }

	private void turn(JSONObject command) {
		String condition = "";
		boolean direction = true;
		int turn = 1;
		double value = 0;
		try {
			condition = command.getString("Condition");
			direction = command.getBoolean("Direction");
			value = command.getInt("Value")*turnRatio;
		} catch(JSONException ex) {
			LCDScreen.drawString("Blad czytania z JSON w navigatorze." +ex.getMessage());
            Sound.beepSequence();
    	}
		if(direction)
			turn = 1;
		else
			turn = -1;
		switch(condition) {
			case "Time":
				int turnSpeed = (int)pilot.getRotateSpeed();
				pilot.rotate(turn * turnSpeed * value, true);
				break;
			case "Angle":
				pilot.rotate(turn * value, true);
				break;
		}
	}
    
    public void stopNavigator() {
    	end = true;
    	pilot.stop();
    }
    
    public static Navigator getInstance() {
        if(instance == null) {
            instance = new Navigator();
            instance.executeQueue = true;
        }
        return instance;
    }

    public void executeCommands(JSONObject commands, boolean atBeginning) throws JSONException {
    	JSONObject command;
    	int count = commands.getInt("Count");
    	if(atBeginning) {
    		if(commandsQueue.isEmpty()) {
    			command = commands.getJSONObject(String.valueOf(count));
    			commandsQueue.push(command);
    		}
	        for(int i=count-1; i>0; i--) {
	            command = commands.getJSONObject(String.valueOf(i));
	        	commandsQueue.insertElementAt(command, 0);
	        }
    	} else {
    		for(int i=0; i<count; i++) {
                command = commands.getJSONObject(String.valueOf(i+1));
            	commandsQueue.push(command);
            }
    	}
    }

    public void interruptSequence(JSONObject commands) throws JSONException {
    	try {
	    	String func = commands.getString("Queue");
	    	String action = commands.getString("Action");
	    	switch(func) {
		    	case "Clear":
		    		commandsQueue.clear();
		    		break;
		    	case "WaitForResume":
		    		executeQueue = false;
		    		break;
		    	case "Resume":
		    		executeQueue = true;
		    		break;
	    	}
	    	switch(action) {
		    	case "Stop":
		    		pilot.stop();
		    		execute = false;
		    		break;
		    	case "Action":
		    		executeCommands(commands.getJSONObject("Perform"), true);
		    		break;
	    	}
	    	sendMessage("I shall execute your request");
    	} catch(JSONException ex) {
    		LCDScreen.drawString("Blad czytania z JSON w przerwaniu." +ex.getMessage());
            Sound.beepSequence();
    	}
    }
    
    private void sendMessage(String msg) {
    	BTOutput.getInstance().setOutputData(msg.getBytes());
    }
    
}
