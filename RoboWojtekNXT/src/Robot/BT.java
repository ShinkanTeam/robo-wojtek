package Robot;

import java.io.DataInputStream;
import java.io.IOException;

import json.JSONException;
import json.JSONObject;
import lejos.nxt.Sound;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

public class BT{
    BTConnection connection = null;
    private static BT instance;
    private DataInputStream dis;
    
    public static BT getInstance() {
        if(instance == null)
            instance = new BT();
        return instance;
    }

    
    public void closeBluetooth() {
    	BTOutput.getInstance().closeStream();
    	connection.close();
    }
    
    public boolean initiate() {
        connection = Bluetooth.waitForConnection(30000, NXTConnection.RAW);
        if(connection == null)
            return false;
        dis = connection.openDataInputStream();
        BTOutput.getInstance().setOutputStream(connection.openDataOutputStream());
        BTOutput.getInstance().start();
        return true;
    }
    
    public JSONObject getNewCommands() {
        return readData();
    }
    
    private JSONObject readData() {
        byte[] partialBuffer = new byte[1024];
        StringBuilder builder = new StringBuilder();
        int bytesReaded = 0;
        do {
        	try {
	            bytesReaded = dis.read(partialBuffer);
	            builder.append(new String(partialBuffer));
        	} catch(IOException ex) {
        		LCDScreen.drawString("Blad czytania z BT. "+ex.getMessage());
	            Sound.beepSequence();
			}
        } while(bytesReaded == 1024);
        return createJSON(builder.toString());
    }

    private JSONObject createJSON(String value) {
        JSONObject json = null;
        try {
        	if(value.equals("")) {
        		json = new JSONObject();
        		json.put("Type", "empty");
        	}
            json = new JSONObject(value);
        } catch (JSONException ex) {
        	LCDScreen.drawString("Blad parsowania do JSON. "+ex.getMessage());
            Sound.beepSequence();
        }
        return json;
    }
}
