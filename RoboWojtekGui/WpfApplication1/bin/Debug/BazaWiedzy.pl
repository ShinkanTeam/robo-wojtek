%pokoje
:- dynamic pokoj/1.

pokoj(korytarz).
pokoj(salon).
pokoj(kuchnia).
pokoj(sypialnia1).
pokoj(sypialnia2).

%drzwi
:- dynamic drzwi/2.

drzwi(korytarz, salon).
drzwi(korytarz, kuchnia).
drzwi(korytarz, sypialnia1).
drzwi(korytarz, sypialnia2).
drzwi(sypialnia2, sypialnia1).

%obiekty znajdujace sie w pokojach
:- dynamic obiekt/1.

obiekt(krzeslo).
obiekt(stol).
obiekt(tapczan).

%lokalizajca danego obiektu
:- dynamic koloru_nalezy_do/3.

koloru_nalezy_do(tapczan, czerwony, sypialnia1).
koloru_nalezy_do(tapczan, brazowy, sypialnia2).
koloru_nalezy_do(stol, brazowy, kuchnia).
koloru_nalezy_do(krzeslo, brazowy, kuchnia).
koloru_nalezy_do(krzeslo, brazowy, kuchnia).
koloru_nalezy_do(krzeslo, brazowy, kuchnia).
koloru_nalezy_do(krzeslo, brazowy, kuchnia).

