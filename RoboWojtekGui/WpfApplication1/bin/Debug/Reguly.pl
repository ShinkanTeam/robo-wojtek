%**********************************
%wczytywanie bazy wiedzy
wczytaj:-
    ['BazaWiedzy.pl'].
    %['C:/.../BazaWiedzy.pl'].

%**********************************
%zapisywanie bazy wiedzy
zapisz:-
    tell('BazaWiedzy.pl'),
    write('%pokoje'),nl,
    listing(pokoj/1),
    write('%drzwi'),nl,
    listing(drzwi/2),
    write('%obiekty znajdujace sie w pokojach'),nl,
    listing(obiekt/1),
    write('%lokalizajca danego obiektu'),nl,
    listing(koloru_nalezy_do/3),
    told.

%**********************************
%podaj droge z jednego pomiesczenia do drugiego

droga(Pokoj1,Pokoj2):-
    append('Droga.txt'),
    (drzwi(Pokoj1,Pokoj2);drzwi(Pokoj2,Pokoj1)),
    write('Z '),write(Pokoj1),write(' do '),write(Pokoj2),nl,
    told.

droga(Pokoj1,Pokoj2):-
    append('Droga.txt'),
    (drzwi(Pokoj1,Y);drzwi(Y,Pokoj1)),
    (drzwi(Y,Pokoj2);drzwi(Pokoj2,Y)),
    write('Z '),write(Pokoj1),write(' przez '),write(Y),write(' do '),write(Pokoj2),nl,
    told.

edytuj:-
edit('Droga.txt').

%**********************************
%sprawdz czy zadany obiekt jest w pokoju

sprawdzwnetrzepokoju(Obiekt,Pokoj):-
    koloru_nalezy_do(Obiekt,_,Pokoj).

%**********************************
%obiekty w pokoju

wszystkieobiektywpokoju(Pokoj):-
    koloru_nalezy_do(Obiekt,Kolor,Pokoj),
    write('W '),write(Pokoj),write(' znajujde sie: '),write(Obiekt),write(' koloru: '),write(Kolor),nl.

%**********************************
%wspolrzedne na mapie (kolejne ruchy)
ruch(A, B, C, D) :-
    krok(A, B, C, E),
    append('mapa.txt'),
    write(A),
    write(','),
    write(B),
    write(;),
    told,
    D=E.

ruch(C, F, A, I) :-
    krok(D, G, A, B),
    ruch(E, H, B, J),
    C is D+E,
    F is G+H,
    I=J.

krok(1, 0, [przod|A], A).
krok(-1, 0, [tyl|A], A).
krok(0, -1, [lewo|A], A).
krok(0, 1, [prawo|A], A).