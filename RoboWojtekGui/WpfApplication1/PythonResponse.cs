﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication1
{
    public class PythonResponse
    {
        public string Result { get; internal set; }
        public string Orders { get; internal set; }
        public string Question { get; internal set; }
        public string NewData { get; internal set; }
        public string Answer { get; internal set; }

        public PythonResponse(string Result, string Orders, string Question, string NewData, string Answer)
        {
            this.Result = Result;
            this.Orders = Orders;
            this.Question = Question;
            this.NewData = NewData;
            this.Answer = Answer;
        }
    }
}
