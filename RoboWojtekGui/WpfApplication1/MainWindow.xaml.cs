﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Scripting.Hosting;
using IronPython.Hosting;
using IronPython.Modules;
using Microsoft.Scripting;
using System.IO.Ports;
using InTheHand.Net;
using InTheHand.Net.Sockets;
using System.IO;
using InTheHand.Net.Bluetooth;
using System.Threading;
using SbsSW.SwiPlCs;
using Newtonsoft.Json;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        class MyConsts
        {
            //Guid do zmiany???
            static readonly Guid MyServiceUuid
              = new Guid("{00112233-4455-6677-8899-aabbccddeeff}");
        }

        BluetoothAddress addr;
        Guid serviceClass;

        BluetoothClient bluetoothCient;
        Stream peerStream;

        DispatcherTimer timer;
        bool isConnected = false;
        ScriptEngine engine;
        Thread t;

        Position position;

        public MainWindow()
        {
            InitializeComponent();
            SetTick();
            position = new Position(0, 0);
            StartProlog();
        }

        #region Timer, ostatecznie nie wiem czy się do czegoś przyda.
        void SetTick()
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(5);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            //DialogueBox.Text += "Robo-Wojtek: " + "...\n";
            //DialogueBox.ScrollToEnd();
        }
        #endregion

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if (QuestionBox.Text != "")
            {
                DialogueBox.Text += "User: " + QuestionBox.Text + "\n";
                DialogueBox.ScrollToEnd();

               // t = new Thread(new ThreadStart(listening));
                //t.Start();

                PythonResponse pythonResult = ExecutePythonScript(@"script.py", QuestionBox.Text);

                if (pythonResult.Answer != "")
                    DialogueBox.Text += "Python: " + pythonResult.Answer + "\n";

                if (isConnected && pythonResult.Orders != "{}")
                    SendMessageToRoboWojtek(pythonResult.Orders);

                if (pythonResult.Question != "")
                {
                    ExecuteProlog(pythonResult.Question);

                    if (File.Exists(@"Droga.txt"))
                    {
                        ReadFromFile(@"Droga.txt");
                    }

                    if (File.Exists(@"Obiekt.txt"))
                    {
                        ReadFromFile(@"Obiekt.txt");
                    }
                }

                if (pythonResult.NewData != "[]")
                {
                    List<string> newData = JsonConvert.DeserializeObject<List<string>>(pythonResult.NewData);

                    foreach (string data in newData)
                        ExecuteProlog(data);
                }

                DialogueBox.ScrollToEnd();
                QuestionBox.Clear();
            }
        }
        
        PythonResponse ExecutePythonScript(string path, string text)
        {
            if (engine == null)
            {
                engine = Python.CreateEngine();

                ICollection<string> Paths = engine.GetSearchPaths();
                Paths.Add(".");
                // Remember about changing dirs to your installation folder of IronPython
                Paths.Add("D:\\DevTools\\IronPython 2.7\\Lib");
                Paths.Add("D:\\DevTools\\IronPython 2.7\\DLLs");
                Paths.Add("D:\\DevTools\\IronPython 2.7");
                Paths.Add("D:\\DevTools\\IronPython 2.7\\lib\\site-packages");

                engine.SetSearchPaths(Paths);
            }

            ScriptSource source = engine.CreateScriptSourceFromFile(path);
            ScriptScope scope = engine.CreateScope();
            scope.SetVariable("text", text);
            source.Execute(scope);

            string orders = "{}";
            string question = "";
            string newData = "[]";
            string answer = "";

            string result = scope.GetVariable("result");
            if (result != "I don't understand what you wrote." && result != "[]")
            {
                orders = scope.GetVariable("orders");
                question = scope.GetVariable("question");
                newData = scope.GetVariable("newData");
            }
            
            if (scope.ContainsVariable("answer"))
                answer = scope.GetVariable("answer");

            return new PythonResponse(result, orders, question, newData, answer);
        }

        void StartProlog()
        {
            string[] param = { "-q" };
            PlEngine.Initialize(param);

            string startupPath = Environment.CurrentDirectory;
            startupPath = startupPath.Replace("\\", "/");

            PlQuery.PlCall("consult('" + startupPath + "/Reguly.pl')");
            PlQuery.PlCall("wczytaj");
        }

        bool ExecuteProlog(string query)
        {
            //string query = "droga(sypialnia2,korytarz)";

            using (PlQuery q = new PlQuery(query))
            {
                foreach (PlQueryVariables v in q.SolutionVariables)
                    DialogueBox.Text += "Prolog: " + "znalazlem!" + "\n"; //(v["G"].ToString()) + "\n";

                
                if (query.Contains("assert") || query.Contains("retract"))
                {   
                }
                else if (query.Contains("zapisz"))
                {
                    DialogueBox.Text += "Prolog: " + "Saved" + "\n"; 
                }
                else if ((query.Contains("pokoj") && !query.Contains("(X)")) || (query.Contains("drzwi") && !query.Contains("X")) ||
                    (query.Contains("obiekt") && !query.Contains("X")) || query.Contains("koloru_nalezy_do"))
                {
                    if (q.SolutionVariables.Count() == 1)
                        DialogueBox.Text += "Prolog: " + "Yes" + "\n";
                    else
                        DialogueBox.Text += "Prolog: " + "No" + "\n";
                }
                else if (query.Contains("ruch"))
                {
                    PlQueryVariables variable = q.SolutionVariables.First();
                    position.Move(int.Parse(variable["X"].ToString()), int.Parse(variable["Y"].ToString()));
                }
                else if (query.Contains("(X)") || (query.Contains("(X,") && !query.Contains("(X,Y)")) || query.Contains(",X)"))
                {
                    string answer = "";
                    foreach (PlQueryVariables v in q.SolutionVariables)
                        answer += v["X"].ToString() + ", ";
                    answer = answer.Substring(0, answer.Length - 2);
                    DialogueBox.Text += "Prolog: " + answer + "\n";
                }
                else if (query.Contains("X,Y"))
                {
                    string answer = "";
                    foreach (PlQueryVariables v in q.SolutionVariables)
                        answer += v["X"].ToString() + ", " + v["Y"].ToString() + "\n";
                    DialogueBox.Text += "Prolog: " + answer;
                }
                else if (query.Contains("sprawdzwnetrzepokoju"))
                {
                    DialogueBox.Text += "Prolog: " + q.SolutionVariables.Count() + "\n";
                }
            }

            return true;
        }

        void ReadFromFile(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);

            string prologResult = "";
            while (!reader.EndOfStream)
                prologResult = reader.ReadLine();
            reader.Close();

            File.Delete(fileName);

            DialogueBox.Text += "Prolog: " + +prologResult + "\n";
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            DialogueBox.Clear();
        }

        private void QuestionBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                SendButton_Click(null, null);
        }

        private void PairButton_Click(object sender, RoutedEventArgs e)
        {
            addr = BluetoothAddress.Parse("00165319F5E2");
            BluetoothSecurity.RemoveDevice(addr);
            if (BluetoothSecurity.PairRequest(addr, "1234"))
                DialogueBox.Text = "Sparowano";
        }

        private void ConnnectButton_Click(object sender, RoutedEventArgs e)
        {
            addr = BluetoothAddress.Parse("00165319F5E2");
            serviceClass = BluetoothService.SerialPort;
            BluetoothEndPoint endPoint = new BluetoothEndPoint(addr, serviceClass);
            bluetoothCient = new BluetoothClient();
            bluetoothCient.Authenticate = true;
            bluetoothCient.Connect(endPoint);
            peerStream = bluetoothCient.GetStream();

            BluetoothRadio.PrimaryRadio.Mode = RadioMode.Discoverable;

            t = new Thread(new ThreadStart(listening));
            t.Start();
        }

        private void listening()
        {
            while (true)
            {
                String message;
                int size = 1024;
                byte[] buffer = new byte[size];
                int read;
                StringBuilder builder = new StringBuilder();
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                do
                {
                    read = peerStream.Read(buffer, 0, size);
                    builder.Append(encoding.GetString(buffer, 0, read));
                    buffer = new byte[size];
                } while (read == size);

                message = builder.ToString();

                DialogueBox.Dispatcher.Invoke(new Action(() => WriteMessageFromRoboWojtek(message)));
            }
        }

        private void WriteMessageFromRoboWojtek(string message)
        {
            DialogueBox.Text += "RoboWojtek: " + message + "\n";
            DialogueBox.ScrollToEnd();
        }

        private void SendMessageToRoboWojtek(string pythonResult)
        {
            Byte[] Message = Encoding.ASCII.GetBytes(pythonResult);
            peerStream.Write(Message, 0, Message.Length);
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            string disc = "{Type: \"Stop\"}";
            SendMessageToRoboWojtek(disc);
            peerStream.Close();
            t.Abort();
        }
    }
}
